import numpy as np
import matplotlib.pyplot as plt


class Kalman_filter:
    def __init__(self, Q, R, initValue=100):
        self.Q = Q
        self.R = R

        self.P_k_k1 = 1
        self.Kg = 0
        self.P_k1_k1 = 1
        self.x_k_k1 = 0
        self.ADC_OLD_Value = 0
        self.Z_k = 0
        self.kalman_adc_old = initValue

    def kalman(self, ADC_Value):
        self.Z_k = ADC_Value
        self.x_k1_k1 = self.kalman_adc_old
        self.x_k_k1 = self.x_k1_k1
        self.P_k_k1 = self.P_k1_k1 + self.Q
        self.Kg = self.P_k_k1 / (self.P_k_k1 + self.R)
        kalman_adc = self.x_k_k1 + self.Kg * (self.Z_k - self.kalman_adc_old)
        self.P_k1_k1 = (1 - self.Kg) * self.P_k_k1
        self.P_k_k1 = self.P_k1_k1
        self.kalman_adc_old = kalman_adc
        return kalman_adc


if __name__ == '__main__':
    kalman_filter = Kalman_filter(0.001, 1)
    a = [100] * 200
    array = np.array(a)

    s = np.random.normal(0, 15, 200)
    test_array = array + s
    adc = []
    for i in range(200):
        adc.append(kalman_filter.kalman(test_array[i]))

    plt.plot(adc)
    plt.plot(array)
    plt.plot(test_array)
    plt.show()