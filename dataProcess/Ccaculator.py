import numpy as np
import pylab as pl
import math
# from data import uc, ic


class CCaculator(object):
    def __init__(self):
        pass

    def myfft(self, y, fs=50000, base_freq=50):
        N = len(y)  # 采样点数 10000
        df = fs / (N - 1)  # 分辨率
        f = [df * n for n in range(0, N)]  # 构建频率数组
        Y = np.fft.fft(y) * 2 / N  # *2/N 反映了FFT变换的结果与实际信号幅值之间的关系
        absY = [np.abs(x) for x in Y]  # 求傅里叶变换结果的模
        index = 0
        for i in f:
            if abs(i - base_freq) < 1:
                index = f.index(i)
                a = absY[index]
                phi = math.atan2(Y[index].imag, Y[index].real) * 180 / math.pi
                return a, phi

    def calculate_C(self, uc, ic, fs=50000, base_freq=50):
        Un, Uphi = self.myfft(y=uc)
        In, Iphi = self.myfft(y=ic)
        esc = -1 / (2 * math.pi * 50 * Un / In * math.sin((Uphi - Iphi) * math.pi / 180))
        return esc * 1000000


if __name__ == '__main__':
    c = CCaculator().calculate_C(uc, ic)
    print(c)
