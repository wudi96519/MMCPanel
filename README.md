# MMCPanel

#### 介绍
项目是一个简单的DSP上位机可视化程序，用于电容值的实时在线检测。使用Python语言，PyQt作为图形界面。
通过串口接收DSP发过来的电流电压数据，计算电容值并显示。
[下载地址](https://gitee.com/wudi96519/MMCPanel/releases)

#### 依赖
pip install 

* pyserial 
* matplotlib
* pyqt5 

#### 修改

###### 修改界面
安装qt designer, 打开Panel/MMC_Panel_UI.ui文件，生成新的.py文件替换MMCPanelUI.py文件。




