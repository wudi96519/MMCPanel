import serial
import serial.tools.list_ports
import time
import matplotlib.pyplot as plt
from threading import Thread
import socket
from PyQt5.QtCore import pyqtSignal, QObject
from dataProcess.Ccaculator import CCaculator
import numpy as np


class SerialHandle(QObject):
    ok_singnal = pyqtSignal()
    recieved_singnal = pyqtSignal()

    def __init__(self, parent=None):
        super(QObject, self).__init__(parent)
        # self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        port_list_ori = list(serial.tools.list_ports.comports())
        self.ireclist = []
        self.vreclist = []
        self.seqreclist = []
        self.old_ireclist = []
        self.old_vreclist = []
        self.old_seqreclist = []
        self.old_Iarm = []
        self.cc = CCaculator()
        self.cc_ok = False
        self.current_c = 0.0

        commname = ''
        if len(port_list_ori) == 1:
            commname = str(port_list_ori[0]).split(" ")[0]
        if len(port_list_ori) > 1:
            for i in port_list_ori:
                if "USB" in str(i):
                    commname = str(i).split(" ")[0]
        if len(commname):
            self._ser = serial.Serial(commname, baudrate=921600)
            self.recived = False
            thr = Thread(target=self.read_thread, name="serial read thread")
            thr.setDaemon(True)
            thr.start()

    def read_thread(self):
        print("start serial read thread")
        while True:
            while True:
                a = self._ser.read(1)
                if a == b'\xaa':
                    break
            tmp = self._ser.read(6)
            # print(tmp[0:2], tmp[2:4], tmp[4])
            irec = ((tmp[0] * 256 + tmp[1]) * -1 + 20.0)
            vrec = ((tmp[2] * 256 + tmp[3]) * 8.13008)
            seqrec = (1 if tmp[4] >= 1 else 0)
            self.ireclist.append(irec)
            self.vreclist.append(vrec)
            self.seqreclist.append(seqrec)
            # time.sleep(0.001)
            # tmp[5] = tmp[5] + 1
            # self._socket.sendto(str(irec).encode(), ("127.0.0.1", 45678))
            # self._socket.sendto(str(vrec).encode(), ("127.0.0.1", 45678))
            # self._socket.sendto(str(seqrec).encode(), ("127.0.0.1", 45678))
            self.recieved_singnal.emit()
            if tmp[4] == 255:
                self.ireclist = []
                self.vreclist = []
                self.seqreclist = []
            if len(self.seqreclist) >= 4999:
                uc = self.vreclist
                ic = np.array(self.seqreclist) * np.array(self.ireclist)
                self.current_c = self.cc.calculate_C(uc=uc, ic=ic)
                # print(self.current_c)
                # plt.plot(ic)
                # plt.show()
                self.ok_singnal.emit()
                self.old_ireclist = self.ireclist
                self.old_vreclist = self.vreclist
                self.old_seqreclist = self.seqreclist
                self.old_Iarm = ic
                self.ireclist = []
                self.vreclist = []
                self.seqreclist = []

                # print("ready")


if __name__ == "__main__":
    serh = SerialHandle()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass
