# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'MMC_Panel_UIeofFsc.ui'
##
## Created by: Qt User Interface Compiler version 5.14.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PyQt5.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint, QLocale,
    QRect, QSize, QUrl, Qt)
from PyQt5.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PyQt5.QtWidgets import *

import pingtai
import yuanli
import xiaohui
import icon_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(989, 766)
        icon = QIcon()
        icon.addFile(u":/icon/icon.ico", QSize(), QIcon.Normal, QIcon.On)
        MainWindow.setWindowIcon(icon)
        MainWindow.setAutoFillBackground(False)
        self.actionViewHistory = QAction(MainWindow)
        self.actionViewHistory.setObjectName(u"actionViewHistory")
        self.actionopenDir = QAction(MainWindow)
        self.actionopenDir.setObjectName(u"actionopenDir")
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.title_label = QLabel(self.centralwidget)
        self.title_label.setObjectName(u"title_label")
        self.title_label.setGeometry(QRect(230, 10, 751, 71))
        font = QFont()
        font.setFamily(u"Segoe UI Emoji")
        font.setPointSize(31)
        font.setBold(True)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setKerning(True)
        self.title_label.setFont(font)
        self.title_label.setAlignment(Qt.AlignCenter)
        self.current_C_label = QLabel(self.centralwidget)
        self.current_C_label.setObjectName(u"current_C_label")
        self.current_C_label.setGeometry(QRect(330, 630, 101, 61))
        font1 = QFont()
        font1.setFamily(u"Agency FB")
        font1.setPointSize(20)
        self.current_C_label.setFont(font1)
        self.current_C_label.setAlignment(Qt.AlignCenter)
        self.lcdNumber = QLCDNumber(self.centralwidget)
        self.lcdNumber.setObjectName(u"lcdNumber")
        self.lcdNumber.setGeometry(QRect(430, 630, 111, 61))
        self.lcdNumber.setProperty("value", 0.000000000000000)
        self.uF_label = QLabel(self.centralwidget)
        self.uF_label.setObjectName(u"uF_label")
        self.uF_label.setGeometry(QRect(550, 630, 71, 61))
        font2 = QFont()
        font2.setFamily(u"Times New Roman")
        font2.setPointSize(36)
        self.uF_label.setFont(font2)
        self.uF_label.setLocale(QLocale(QLocale.Chinese, QLocale.China))
        self.progressBar = QProgressBar(self.centralwidget)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setGeometry(QRect(330, 700, 301, 23))
        self.progressBar.setValue(0)
        self.gridLayoutWidget = QWidget(self.centralwidget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(670, 100, 301, 631))
        self.plt3 = QGridLayout(self.gridLayoutWidget)
        self.plt3.setObjectName(u"plt3")
        self.plt3.setContentsMargins(0, 0, 0, 0)
        self.ave_U = QLabel(self.centralwidget)
        self.ave_U.setObjectName(u"ave_U")
        self.ave_U.setGeometry(QRect(330, 580, 201, 31))
        self.ave_U.setFont(font1)
        self.ave_U.setAutoFillBackground(False)
        self.ave_U.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)
        self.ave_U_Text = QLabel(self.centralwidget)
        self.ave_U_Text.setObjectName(u"ave_U_Text")
        self.ave_U_Text.setGeometry(QRect(540, 580, 71, 41))
        self.ave_U_Text.setFont(font1)
        self.ave_U_Text.setMouseTracking(False)
        self.ave_U_Text.setAutoFillBackground(False)
        self.ave_U_Text.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)
        self.currentTime = QLabel(self.centralwidget)
        self.currentTime.setObjectName(u"currentTime")
        self.currentTime.setGeometry(QRect(20, 690, 291, 41))
        font3 = QFont()
        font3.setFamily(u"Agency FB")
        font3.setPointSize(22)
        self.currentTime.setFont(font3)
        self.bimg = QLabel(self.centralwidget)
        self.bimg.setObjectName(u"bimg")
        self.bimg.setGeometry(QRect(10, -10, 221, 121))
        self.bimg.setStyleSheet(u"image: url(:/xiaohui2/xiaohui2.jpg);\n"
"image: url(:/xiaohui/xiaohui.jpg);")
        self.bimg_2 = QLabel(self.centralwidget)
        self.bimg_2.setObjectName(u"bimg_2")
        self.bimg_2.setGeometry(QRect(10, 70, 271, 641))
        self.bimg_2.setStyleSheet(u"image: url(:/xiaohui/xiaohui.jpg);\n"
"image: url(:/pingtai/pingtai.png);")
        self.bimg_3 = QLabel(self.centralwidget)
        self.bimg_3.setObjectName(u"bimg_3")
        self.bimg_3.setGeometry(QRect(310, 90, 331, 211))
        self.bimg_3.setAutoFillBackground(False)
        self.bimg_3.setStyleSheet(u"image: url(:/xiaohui2/xiaohui2.jpg);\n"
"image: url(:/yuanli/yuanli.png);")
        self.gridLayoutWidget_2 = QWidget(self.centralwidget)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(300, 310, 361, 241))
        self.plt1 = QGridLayout(self.gridLayoutWidget_2)
        self.plt1.setObjectName(u"plt1")
        self.plt1.setContentsMargins(0, 0, 0, 0)
        self.graphicsView = QGraphicsView(self.centralwidget)
        self.graphicsView.setObjectName(u"graphicsView")
        self.graphicsView.setGeometry(QRect(-25, -9, 1051, 791))
        MainWindow.setCentralWidget(self.centralwidget)
        self.graphicsView.raise_()
        self.bimg.raise_()
        self.bimg_2.raise_()
        self.title_label.raise_()
        self.current_C_label.raise_()
        self.lcdNumber.raise_()
        self.uF_label.raise_()
        self.progressBar.raise_()
        self.gridLayoutWidget.raise_()
        self.ave_U.raise_()
        self.ave_U_Text.raise_()
        self.currentTime.raise_()
        self.bimg_3.raise_()
        self.gridLayoutWidget_2.raise_()
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 989, 23))
        self.menu = QMenu(self.menubar)
        self.menu.setObjectName(u"menu")
        MainWindow.setMenuBar(self.menubar)

        self.menubar.addAction(self.menu.menuAction())
        self.menu.addAction(self.actionViewHistory)
        self.menu.addAction(self.actionopenDir)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"\u7535\u5bb9\u7cbe\u7075", None))
        self.actionViewHistory.setText(QCoreApplication.translate("MainWindow", u"\u7535\u5bb9\u5386\u53f2\u6570\u636e", None))
        self.actionopenDir.setText(QCoreApplication.translate("MainWindow", u"\u6253\u5f00\u6570\u636e\u8def\u5f84", None))
        self.title_label.setText(QCoreApplication.translate("MainWindow", u"MMC\u5b50\u6a21\u5757\u7535\u5bb9\u5728\u7ebf\u68c0\u6d4b\u7cfb\u7edfVER1.02", None))
        self.current_C_label.setText(QCoreApplication.translate("MainWindow", u"\u7535\u5bb9\u503c\uff1a", None))
        self.uF_label.setText(QCoreApplication.translate("MainWindow", u"\u03bcF", None))
        self.ave_U.setText(QCoreApplication.translate("MainWindow", u"\u7535\u5bb9\u7535\u538b\u5e73\u5747\u503c\uff1a", None))
        self.ave_U_Text.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.currentTime.setText("")
        self.bimg.setText("")
        self.bimg_2.setText("")
        self.bimg_3.setText("")
        self.menu.setTitle(QCoreApplication.translate("MainWindow", u"\u67e5\u770b", None))
    # retranslateUi

