#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import time
# from shutil import copyfile
import sys


class DataRecorder(object):
    def __init__(self,path = "test.csv"):
        self.filepath = path
        self.copypath = path

    def touch(self, file_name, file_path):
        os.chdir(file_path)
        if file_name in os.listdir('.'):
            print("file exist!")
        else:
            print("creating %s" % file_name)
            fid = open(file_name, 'w')
            fid.write("time,ave_u,ave_i,c\n")
            fid.close()

    def record_data(self, ave_uc, ave_ic, c):
        ex = os.path.exists(self.filepath)
        # print(ex)
        if not ex:
            self.touch(self.filepath, "./")
        with open(self.filepath, 'a') as f:
            f.write("{},{},{},{}\n".format(time.asctime(time.localtime(time.time())), ave_uc, ave_ic, c))


    # copyfile(source, target)


if __name__ == '__main__':
    dr = DataRecorder()
    dr.record_data(100,0.5,322)







