# -*- coding:utf-8 -*-
import sys
import os
from PyQt5.QtWidgets import QApplication, QMainWindow
import webbrowser

from Panel.MMCPanelUi import Ui_MainWindow
from serialTool.serialHandle import SerialHandle
from threading import Thread
import matplotlib
import time

matplotlib.use("Qt5Agg")  # 声明使用QT5
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FC
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
from excelTool.excelTool import DataRecorder
from shutil import copyfile
from win32com.client import Dispatch
from dataProcess.kalman import Kalman_filter
from matplotlib.pyplot import MultipleLocator


class MyMainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MyMainWindow, self).__init__(parent)
        self.init_time = str(format(time.asctime(time.localtime(time.time()))))
        self.init_time = self.init_time.replace(":", ' ')
        self.datafilepath = self.init_time + ".csv"
        self.setupUi(self)
        self.serialHandle = SerialHandle()
        self.cap_num_to_plot = 400
        self.kalman_filter = Kalman_filter(0.001, 1, initValue=342.0)
        self.cap_ori_list = []
        self.cap_cal_list = []
        self.serialHandle.ok_singnal.connect(self.update_curve_and_C)
        self.serialHandle.recieved_singnal.connect(self.update_process_bar)
        self.fig_1 = plt.Figure()
        self.fig_2 = plt.Figure()
        self.view1 = FC(self.fig_1)
        self.view1.raise_()
        self.view2 = FC(self.fig_2)
        self.view2.raise_()
        self.fig1 = self.fig_2.add_subplot(3, 1, 1)
        self.fig2 = self.fig_2.add_subplot(3, 1, 2)
        self.fig3 = self.fig_2.add_subplot(3, 1, 3)
        self.fig4 = self.fig_1.add_subplot(1, 1, 1)
        self.fig4.set_title("C")
        self.fig1.set_title("I(arm)")
        self.fig2.set_title("U(cap)")
        self.fig3.set_title("I(cap)")
        self.fig_2.set_tight_layout("10")
        self.plt1.addWidget((self.view1))
        self.plt3.addWidget((self.view2))
        self.actionViewHistory.triggered.connect(self.show_history_data)
        self.actionopenDir.triggered.connect(self.open_file_dir)
        self.dr = DataRecorder(self.datafilepath)
        thr = Thread(target=self.serialHandle.read_thread, )
        thr.setDaemon(True)
        thr.start()

    def open_file_dir(self):
        os.system("explorer.exe %s" % os.getcwd())

    def show_history_data(self):
        copyfile(self.datafilepath, 'tmpdata.csv')
        webbrowser.open('tmpdata.csv')

    def update_curve_and_C(self):
        # print("ok")
        try:
            self.fig1.cla()
            self.fig2.cla()
            self.fig3.cla()
            self.fig4.cla()
            x_major_locator = MultipleLocator(0.02)
            y_major_locator = MultipleLocator(10)
            self.fig1.xaxis.set_major_locator(x_major_locator)
            self.fig2.xaxis.set_major_locator(x_major_locator)
            self.fig3.xaxis.set_major_locator(x_major_locator)
            self.fig4.yaxis.set_major_locator(y_major_locator)
            self.fig1.plot(np.array(range(len(self.serialHandle.old_vreclist)))/50000.0, self.serialHandle.old_ireclist)
            self.fig1.set_title("I(arm)")
            self.fig2.plot(np.array(range(len(self.serialHandle.old_vreclist)))/50000.0, self.serialHandle.old_vreclist)
            self.fig2.set_title("U(cap)")
            self.fig3.plot(np.array(range(len(self.serialHandle.old_vreclist)))/50000.0, np.array(self.serialHandle.old_Iarm))
            self.fig3.set_title("I(cap)")

            self.view2.draw()
            tmp_c = self.serialHandle.current_c
            self.cap_ori_list.append(tmp_c)
            kalman_c = self.kalman_filter.kalman(tmp_c)
            self.lcdNumber.display(kalman_c)
            self.cap_cal_list.append(kalman_c)
            if len(self.cap_cal_list) > self.cap_num_to_plot:
                del self.cap_ori_list[0]
                del self.cap_cal_list[0]
            self.fig4.plot(range(len(self.cap_ori_list)), np.array(self.cap_ori_list),
                           range(len(self.cap_cal_list)), np.array(self.cap_cal_list))
            self.fig4.set_title("C")
            self.view1.draw()
            ave_u = np.average(self.serialHandle.old_vreclist)
            ave_ic = np.average(self.serialHandle.old_Iarm)
            self.ave_U_Text.setText("{:.2f}".format(ave_u))
            self.dr.record_data(ave_u, ave_ic, kalman_c)

        except Exception as e:
            print(e)

    def update_process_bar(self):
        self.progressBar.setValue(int(len(self.serialHandle.ireclist) / 50))
        self.currentTime.setText(str(format(time.asctime(time.localtime(time.time())))))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWin = MyMainWindow()
    myWin.show()
    sys.exit(app.exec_())
